package crmtests;

import net.sf.jni4net.Bridge;
import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.protocol.java.sampler.JavaSamplerClient;
import org.apache.jmeter.protocol.java.sampler.JavaSamplerContext;
import org.apache.jmeter.samplers.SampleResult;
import system.io.IOException;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BaseBridge implements JavaSamplerClient
{

    protected SampleResult testResult;
    protected java.lang.String arg1;
    protected boolean testFailed = false;
    protected java.lang.String testMessage = "";

    public static boolean isInitialised()
    {
        return initialised;
    }

    static boolean initialised = false;

    public void setupTest(JavaSamplerContext javaSamplerContext)
    {
        if (!initialised) {
            System.out.println("setupTest");

            System.out.println("Working Directory = " +
                    System.getProperty("user.dir"));

            // Get all files from user.dir
            File folder = new File(System.getProperty("user.dir") + "\\libs");
            File[] listOfFiles = folder.listFiles();

            // List of ignored files
            List<java.lang.String> skipNames = Arrays.asList("Test_testJmeterJavaSamplerJni.dll");

            // Get all libs without ignored with extended
            List<File> libs = new ArrayList();
            for (File file : listOfFiles) {
                if (file.isFile()) {
                    if (file.getName().endsWith(".dll") && !skipNames.contains(file.getName()))
                        libs.add(file);
                }
            }

            try {
                Bridge.setVerbose(true);
                Bridge.setDebug(true);
                Bridge.init(new File(System.getProperty("user.dir") + "\\libs"));

                for (File lib : libs) {
                    try {
                        Bridge.LoadAndRegisterAssemblyFrom(lib);
                    } catch (system.ArgumentException se) {
                        System.out.println("javaSamplerContext = [" + se + "]");
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (java.io.IOException e) {
                e.printStackTrace();
            }
            initialised = true;
        }

        testResult = new SampleResult();
        testResult.setSamplerData(toString());
        testResult.setDataType("text");
        testResult.setContentType("text/plain");
        testResult.setDataEncoding("UTF-8");

        testResult.setSuccessful(true);
        testResult.setResponseMessageOK();
        testResult.setResponseCodeOK();

        if (null != javaSamplerContext)
        {
            arg1 = javaSamplerContext.getParameter("arg1", "");
            if (arg1 != null && arg1.length() > 0) {
                testResult.setSamplerData(arg1);
            }
        }
    }

    public SampleResult runTest(JavaSamplerContext javaSamplerContext)
    {
        return testResult;
    }

    public void teardownTest(JavaSamplerContext javaSamplerContext)
    {
        System.out.println("tearDown");
    }

    public Arguments getDefaultParameters()
    {
        System.out.println("getDefaultParameters");

        Arguments params = new Arguments();
        params.addArgument("arg1", "abc"); //Define a parameter, according to parameter list in Jmeter, display name, the first parameter is the default parameters, second parameters to the default value making
        return params;
    }
}