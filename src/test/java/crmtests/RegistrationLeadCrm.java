package crmtests;

import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.protocol.java.sampler.JavaSamplerContext;
import org.apache.jmeter.samplers.SampleResult;
import performancetests.registration.RegistrationLeadFromCrm;

public class RegistrationLeadCrm extends BaseBridge
{
    public static void main(String[] args)
    {
        RegistrationLeadCrm sampler = new RegistrationLeadCrm();
        sampler.setupTest(null);

        Arguments args_ = new Arguments();
        args_.addArgument("arg1", "qwerty");
        SampleResult res = sampler.runTest(new JavaSamplerContext(args_));
        long ttl_diff = res.getEndTime() - res.getStartTime();
        System.out.println("SampleResult = [" + res.getStartTime() + " ==> " + res.getEndTime() + "] : " + ttl_diff);
    }

    @Override
    public SampleResult runTest(JavaSamplerContext javaSamplerContext)
    {
        //Defines a transaction, said this is a starting point of transaction, similar to the LoadRunner making
        testResult.sampleStart();
        System.out.println("Working Directory = " +
                System.getProperty("user.dir"));

        try
        {
            //Call methods from .Net project
            new RegistrationLeadFromCrm().RegLeadWithOutPlatform();
        }

        catch (Exception exx)
        {
            exx.printStackTrace();
            testFailed = true;
            testMessage = exx.getMessage();
        }
        finally
        {
            //Defines a transaction, says this is the end point of transaction, similar to the LoadRunner making
            testResult.sampleEnd();
        }
        if (testFailed)
        {
            testResult.setSuccessful(false);
            testResult.setResponseMessage(testMessage);
        }
        return testResult;
    }
}