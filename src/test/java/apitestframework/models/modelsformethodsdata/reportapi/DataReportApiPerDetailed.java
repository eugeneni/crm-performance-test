// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by jni4net. See http://jni4net.sourceforge.net/ 
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

package apitestframework.models.modelsformethodsdata.reportapi;

@net.sf.jni4net.attributes.ClrType
public class DataReportApiPerDetailed extends system.Object {
    
    //<generated-proxy>
    private static system.Type staticType;
    
    protected DataReportApiPerDetailed(net.sf.jni4net.inj.INJEnv __env, long __handle) {
            super(__env, __handle);
    }
    
    @net.sf.jni4net.attributes.ClrConstructor("()V")
    public DataReportApiPerDetailed() {
            super(((net.sf.jni4net.inj.INJEnv)(null)), 0);
        apitestframework.models.modelsformethodsdata.reportapi.DataReportApiPerDetailed.__ctorDataReportApiPerDetailed0(this);
    }
    
    @net.sf.jni4net.attributes.ClrMethod("()V")
    private native static void __ctorDataReportApiPerDetailed0(net.sf.jni4net.inj.IClrProxy thiz);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getDateStart();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setDateStart(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getDateEnd();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setDateEnd(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getSortBy();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setSortBy(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getActivationDateStart();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setActivationDateStart(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getActivationDateEnd();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setActivationDateEnd(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getEmail();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setEmail(java.lang.String value);
    
    public static system.Type typeof() {
        return apitestframework.models.modelsformethodsdata.reportapi.DataReportApiPerDetailed.staticType;
    }
    
    private static void InitJNI(net.sf.jni4net.inj.INJEnv env, system.Type staticType) {
        apitestframework.models.modelsformethodsdata.reportapi.DataReportApiPerDetailed.staticType = staticType;
    }
    //</generated-proxy>
}
