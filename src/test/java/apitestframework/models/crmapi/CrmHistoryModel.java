// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by jni4net. See http://jni4net.sourceforge.net/ 
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

package apitestframework.models.crmapi;

@net.sf.jni4net.attributes.ClrType
public class CrmHistoryModel extends system.Object {
    
    //<generated-proxy>
    private static system.Type staticType;
    
    protected CrmHistoryModel(net.sf.jni4net.inj.INJEnv __env, long __handle) {
            super(__env, __handle);
    }
    
    @net.sf.jni4net.attributes.ClrConstructor("()V")
    public CrmHistoryModel() {
            super(((net.sf.jni4net.inj.INJEnv)(null)), 0);
        apitestframework.models.crmapi.CrmHistoryModel.__ctorCrmHistoryModel0(this);
    }
    
    @net.sf.jni4net.attributes.ClrMethod("()V")
    private native static void __ctorCrmHistoryModel0(net.sf.jni4net.inj.IClrProxy thiz);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getId();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setId(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getLeadId();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setLeadId(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()I")
    public native int getHistoryTypeId();
    
    @net.sf.jni4net.attributes.ClrMethod("(I)V")
    public native void setHistoryTypeId(int value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/DateTime;")
    public native system.DateTime getCrmCreated();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/DateTime;)V")
    public native void setCrmCreated(system.DateTime value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getSalesStatusId();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setSalesStatusId(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getComment();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setComment(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getUserId();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setUserId(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getUserName();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setUserName(java.lang.String value);
    
    public static system.Type typeof() {
        return apitestframework.models.crmapi.CrmHistoryModel.staticType;
    }
    
    private static void InitJNI(net.sf.jni4net.inj.INJEnv env, system.Type staticType) {
        apitestframework.models.crmapi.CrmHistoryModel.staticType = staticType;
    }
    //</generated-proxy>
}
