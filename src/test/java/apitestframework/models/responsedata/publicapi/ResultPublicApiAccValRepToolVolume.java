// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by jni4net. See http://jni4net.sourceforge.net/ 
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

package apitestframework.models.responsedata.publicapi;

@net.sf.jni4net.attributes.ClrType
public class ResultPublicApiAccValRepToolVolume extends system.Object {
    
    //<generated-proxy>
    private static system.Type staticType;
    
    protected ResultPublicApiAccValRepToolVolume(net.sf.jni4net.inj.INJEnv __env, long __handle) {
            super(__env, __handle);
    }
    
    @net.sf.jni4net.attributes.ClrConstructor("()V")
    public ResultPublicApiAccValRepToolVolume() {
            super(((net.sf.jni4net.inj.INJEnv)(null)), 0);
        apitestframework.models.responsedata.publicapi.ResultPublicApiAccValRepToolVolume.__ctorResultPublicApiAccValRepToolVolume0(this);
    }
    
    @net.sf.jni4net.attributes.ClrMethod("()V")
    private native static void __ctorResultPublicApiAccValRepToolVolume0(net.sf.jni4net.inj.IClrProxy thiz);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/Decimal;")
    public native system.Decimal getVolumEPBC();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/Decimal;)V")
    public native void setVolumEPBC(system.Decimal value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/Decimal;")
    public native system.Decimal getDealSPLPBC();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/Decimal;)V")
    public native void setDealSPLPBC(system.Decimal value);
    
    public static system.Type typeof() {
        return apitestframework.models.responsedata.publicapi.ResultPublicApiAccValRepToolVolume.staticType;
    }
    
    private static void InitJNI(net.sf.jni4net.inj.INJEnv env, system.Type staticType) {
        apitestframework.models.responsedata.publicapi.ResultPublicApiAccValRepToolVolume.staticType = staticType;
    }
    //</generated-proxy>
}
