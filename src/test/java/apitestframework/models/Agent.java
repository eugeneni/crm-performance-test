// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by jni4net. See http://jni4net.sourceforge.net/ 
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

package apitestframework.models;

@net.sf.jni4net.attributes.ClrType
public class Agent extends system.Object {
    
    //<generated-proxy>
    private static system.Type staticType;
    
    protected Agent(net.sf.jni4net.inj.INJEnv __env, long __handle) {
            super(__env, __handle);
    }
    
    @net.sf.jni4net.attributes.ClrConstructor("()V")
    public Agent() {
            super(((net.sf.jni4net.inj.INJEnv)(null)), 0);
        apitestframework.models.Agent.__ctorAgent0(this);
    }
    
    @net.sf.jni4net.attributes.ClrMethod("()V")
    private native static void __ctorAgent0(net.sf.jni4net.inj.IClrProxy thiz);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getId();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setId(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getName();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setName(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getOrder();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setOrder(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()Z")
    public native boolean isAssignable();
    
    @net.sf.jni4net.attributes.ClrMethod("(Z)V")
    public native void setIsAssignable(boolean value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getBusinessUnitId();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setBusinessUnitId(java.lang.String value);
    
    public static system.Type typeof() {
        return apitestframework.models.Agent.staticType;
    }
    
    private static void InitJNI(net.sf.jni4net.inj.INJEnv env, system.Type staticType) {
        apitestframework.models.Agent.staticType = staticType;
    }
    //</generated-proxy>
}
