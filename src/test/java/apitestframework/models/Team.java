// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by jni4net. See http://jni4net.sourceforge.net/ 
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

package apitestframework.models;

@net.sf.jni4net.attributes.ClrType
public class Team extends system.Object {
    
    //<generated-proxy>
    private static system.Type staticType;
    
    protected Team(net.sf.jni4net.inj.INJEnv __env, long __handle) {
            super(__env, __handle);
    }
    
    @net.sf.jni4net.attributes.ClrConstructor("()V")
    public Team() {
            super(((net.sf.jni4net.inj.INJEnv)(null)), 0);
        apitestframework.models.Team.__ctorTeam0(this);
    }
    
    @net.sf.jni4net.attributes.ClrMethod("()V")
    private native static void __ctorTeam0(net.sf.jni4net.inj.IClrProxy thiz);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getId();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setId(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getName();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setName(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getDepartmentId();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setDepartmentId(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getBusinessUnitId();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setBusinessUnitId(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/Object;")
    public native system.Object getActiveMembers();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/Object;)V")
    public native void setActiveMembers(system.Object value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getLocationId();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setLocationId(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()[LApiTestFramework/Models/Manager;")
    public native apitestframework.models.Manager[] getManagers();
    
    @net.sf.jni4net.attributes.ClrMethod("([LApiTestFramework/Models/Manager;)V")
    public native void setManagers(apitestframework.models.Manager[] value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getAssignedLeadsCount();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setAssignedLeadsCount(java.lang.String value);
    
    public static system.Type typeof() {
        return apitestframework.models.Team.staticType;
    }
    
    private static void InitJNI(net.sf.jni4net.inj.INJEnv env, system.Type staticType) {
        apitestframework.models.Team.staticType = staticType;
    }
    //</generated-proxy>
}
