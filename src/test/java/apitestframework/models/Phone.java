// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by jni4net. See http://jni4net.sourceforge.net/ 
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

package apitestframework.models;

@net.sf.jni4net.attributes.ClrType
public class Phone extends system.Object {
    
    //<generated-proxy>
    private static system.Type staticType;
    
    protected Phone(net.sf.jni4net.inj.INJEnv __env, long __handle) {
            super(__env, __handle);
    }
    
    @net.sf.jni4net.attributes.ClrConstructor("()V")
    public Phone() {
            super(((net.sf.jni4net.inj.INJEnv)(null)), 0);
        apitestframework.models.Phone.__ctorPhone0(this);
    }
    
    @net.sf.jni4net.attributes.ClrMethod("()V")
    private native static void __ctorPhone0(net.sf.jni4net.inj.IClrProxy thiz);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getCountry();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setCountry(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getOperator();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setOperator(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getVisiblePart();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setVisiblePart(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getInvisiblePart();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setInvisiblePart(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("(LApiTestFramework/Models/Phone;)Z")
    public native boolean Equals(apitestframework.models.Phone phone);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String GetFullPhone();
    
    public static system.Type typeof() {
        return apitestframework.models.Phone.staticType;
    }
    
    private static void InitJNI(net.sf.jni4net.inj.INJEnv env, system.Type staticType) {
        apitestframework.models.Phone.staticType = staticType;
    }
    //</generated-proxy>
}
