// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by jni4net. See http://jni4net.sourceforge.net/ 
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

package apitestframework.models;

@net.sf.jni4net.attributes.ClrType
public class Country extends system.Object implements system.IComparable {
    
    //<generated-proxy>
    private static system.Type staticType;
    
    protected Country(net.sf.jni4net.inj.INJEnv __env, long __handle) {
            super(__env, __handle);
    }
    
    @net.sf.jni4net.attributes.ClrConstructor("()V")
    public Country() {
            super(((net.sf.jni4net.inj.INJEnv)(null)), 0);
        apitestframework.models.Country.__ctorCountry0(this);
    }
    
    @net.sf.jni4net.attributes.ClrMethod("()V")
    private native static void __ctorCountry0(net.sf.jni4net.inj.IClrProxy thiz);
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/Object;)I")
    public native int CompareTo(system.Object obj);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getId();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setId(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getName();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setName(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getCountryCode();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setCountryCode(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getCountryIsoCode();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setCountryIsoCode(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getPhoneCode();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setPhoneCode(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()[I")
    public native int[] getPhoneCodes();
    
    @net.sf.jni4net.attributes.ClrMethod("([I)V")
    public native void setPhoneCodes(int[] value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getOrder();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setOrder(java.lang.String value);
    
    public static system.Type typeof() {
        return apitestframework.models.Country.staticType;
    }
    
    private static void InitJNI(net.sf.jni4net.inj.INJEnv env, system.Type staticType) {
        apitestframework.models.Country.staticType = staticType;
    }
    //</generated-proxy>
}
