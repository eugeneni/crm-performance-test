// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by jni4net. See http://jni4net.sourceforge.net/ 
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

package apitestframework.models;

@net.sf.jni4net.attributes.ClrType
public class Role extends system.Object {
    
    //<generated-proxy>
    private static system.Type staticType;
    
    protected Role(net.sf.jni4net.inj.INJEnv __env, long __handle) {
            super(__env, __handle);
    }
    
    @net.sf.jni4net.attributes.ClrConstructor("()V")
    public Role() {
            super(((net.sf.jni4net.inj.INJEnv)(null)), 0);
        apitestframework.models.Role.__ctorRole0(this);
    }
    
    @net.sf.jni4net.attributes.ClrMethod("()V")
    private native static void __ctorRole0(net.sf.jni4net.inj.IClrProxy thiz);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getId();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setId(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getName();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setName(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()[LSystem/String;")
    public native java.lang.String[] getBusinessUnit();
    
    @net.sf.jni4net.attributes.ClrMethod("([LSystem/String;)V")
    public native void setBusinessUnit(java.lang.String[] value);
    
    @net.sf.jni4net.attributes.ClrMethod("()Z")
    public native boolean isSystem();
    
    @net.sf.jni4net.attributes.ClrMethod("(Z)V")
    public native void setIsSystem(boolean value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getTeam();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setTeam(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getTeamId();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setTeamId(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getDepartment();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setDepartment(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getDepartmentId();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setDepartmentId(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getUniqueId();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setUniqueId(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getRoleTypeId();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setRoleTypeId(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getRoleTypeName();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setRoleTypeName(java.lang.String value);
    
    public static system.Type typeof() {
        return apitestframework.models.Role.staticType;
    }
    
    private static void InitJNI(net.sf.jni4net.inj.INJEnv env, system.Type staticType) {
        apitestframework.models.Role.staticType = staticType;
    }
    //</generated-proxy>
}
