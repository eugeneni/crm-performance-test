// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by jni4net. See http://jni4net.sourceforge.net/ 
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

package apitestframework.models.forsendingrequests;

@net.sf.jni4net.attributes.ClrType
public class SearchData extends system.Object {
    
    //<generated-proxy>
    private static system.Type staticType;
    
    protected SearchData(net.sf.jni4net.inj.INJEnv __env, long __handle) {
            super(__env, __handle);
    }
    
    @net.sf.jni4net.attributes.ClrConstructor("()V")
    public SearchData() {
            super(((net.sf.jni4net.inj.INJEnv)(null)), 0);
        apitestframework.models.forsendingrequests.SearchData.__ctorSearchData0(this);
    }
    
    @net.sf.jni4net.attributes.ClrMethod("()V")
    private native static void __ctorSearchData0(net.sf.jni4net.inj.IClrProxy thiz);
    
    @net.sf.jni4net.attributes.ClrMethod("()LApiTestFramework/Models/ForSendingRequests/SearchPaging;")
    public native apitestframework.models.forsendingrequests.SearchPaging getPaging();
    
    @net.sf.jni4net.attributes.ClrMethod("(LApiTestFramework/Models/ForSendingRequests/SearchPaging;)V")
    public native void setPaging(apitestframework.models.forsendingrequests.SearchPaging value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LApiTestFramework/Models/ForSendingRequests/SearchFiltering;")
    public native apitestframework.models.forsendingrequests.SearchFiltering getFiltering();
    
    @net.sf.jni4net.attributes.ClrMethod("(LApiTestFramework/Models/ForSendingRequests/SearchFiltering;)V")
    public native void setFiltering(apitestframework.models.forsendingrequests.SearchFiltering value);
    
    public static system.Type typeof() {
        return apitestframework.models.forsendingrequests.SearchData.staticType;
    }
    
    private static void InitJNI(net.sf.jni4net.inj.INJEnv env, system.Type staticType) {
        apitestframework.models.forsendingrequests.SearchData.staticType = staticType;
    }
    //</generated-proxy>
}
