// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by jni4net. See http://jni4net.sourceforge.net/ 
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

package apitestframework.models.forsendingrequests;

@net.sf.jni4net.attributes.ClrType
public class Filter extends system.Object {
    
    //<generated-proxy>
    private static system.Type staticType;
    
    protected Filter(net.sf.jni4net.inj.INJEnv __env, long __handle) {
            super(__env, __handle);
    }
    
    @net.sf.jni4net.attributes.ClrConstructor("(LSystem/String;LSystem/String;LSystem/String;)V")
    public Filter(java.lang.String column, java.lang.String condition, java.lang.String value) {
            super(((net.sf.jni4net.inj.INJEnv)(null)), 0);
        apitestframework.models.forsendingrequests.Filter.__ctorFilter0(this, column, condition, value);
    }
    
    @net.sf.jni4net.attributes.ClrMethod("(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V")
    private native static void __ctorFilter0(net.sf.jni4net.inj.IClrProxy thiz, java.lang.String column, java.lang.String condition, java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getCondition();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setCondition(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getValue();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setValue(java.lang.String value);
    
    @net.sf.jni4net.attributes.ClrMethod("()LSystem/String;")
    public native java.lang.String getColumn();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)V")
    public native void setColumn(java.lang.String value);
    
    public static system.Type typeof() {
        return apitestframework.models.forsendingrequests.Filter.staticType;
    }
    
    private static void InitJNI(net.sf.jni4net.inj.INJEnv env, system.Type staticType) {
        apitestframework.models.forsendingrequests.Filter.staticType = staticType;
    }
    //</generated-proxy>
}
