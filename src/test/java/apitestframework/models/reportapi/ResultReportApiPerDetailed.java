// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by jni4net. See http://jni4net.sourceforge.net/ 
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

package apitestframework.models.reportapi;

@net.sf.jni4net.attributes.ClrType
public class ResultReportApiPerDetailed extends system.Object {
    
    //<generated-proxy>
    private static system.Type staticType;
    
    protected ResultReportApiPerDetailed(net.sf.jni4net.inj.INJEnv __env, long __handle) {
            super(__env, __handle);
    }
    
    @net.sf.jni4net.attributes.ClrConstructor("()V")
    public ResultReportApiPerDetailed() {
            super(((net.sf.jni4net.inj.INJEnv)(null)), 0);
        apitestframework.models.reportapi.ResultReportApiPerDetailed.__ctorResultReportApiPerDetailed0(this);
    }
    
    @net.sf.jni4net.attributes.ClrMethod("()V")
    private native static void __ctorResultReportApiPerDetailed0(net.sf.jni4net.inj.IClrProxy thiz);
    
    public static system.Type typeof() {
        return apitestframework.models.reportapi.ResultReportApiPerDetailed.staticType;
    }
    
    private static void InitJNI(net.sf.jni4net.inj.INJEnv env, system.Type staticType) {
        apitestframework.models.reportapi.ResultReportApiPerDetailed.staticType = staticType;
    }
    //</generated-proxy>
}
