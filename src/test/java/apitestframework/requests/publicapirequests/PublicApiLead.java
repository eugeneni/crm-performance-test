// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by jni4net. See http://jni4net.sourceforge.net/ 
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

package apitestframework.requests.publicapirequests;

@net.sf.jni4net.attributes.ClrType
public class PublicApiLead extends system.Object {
    
    //<generated-proxy>
    private static system.Type staticType;
    
    protected PublicApiLead(net.sf.jni4net.inj.INJEnv __env, long __handle) {
            super(__env, __handle);
    }
    
    @net.sf.jni4net.attributes.ClrConstructor("()V")
    public PublicApiLead() {
            super(((net.sf.jni4net.inj.INJEnv)(null)), 0);
        apitestframework.requests.publicapirequests.PublicApiLead.__ctorPublicApiLead0(this);
    }
    
    @net.sf.jni4net.attributes.ClrMethod("()V")
    private native static void __ctorPublicApiLead0(net.sf.jni4net.inj.IClrProxy thiz);
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;LSystem/String;)LRestSharp/IRestResponse;")
    public native system.Object SendRequestGender(java.lang.String leadId, java.lang.String gender);
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;LSystem/DateTime;)LRestSharp/IRestResponse;")
    public native system.Object SendRequestBirthday(java.lang.String leadId, system.DateTime birthday);
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;LSystem/String;LSystem/String;)LRestSharp/IRestResponse;")
    public native system.Object SendRequestPhone1(java.lang.String leadId, java.lang.String countryCode, java.lang.String number);
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;LSystem/String;LSystem/String;)LRestSharp/IRestResponse;")
    public native system.Object SendRequestPhone2(java.lang.String leadId, java.lang.String countryCode, java.lang.String number);
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;LSystem/String;LSystem/String;)LRestSharp/IRestResponse;")
    public native system.Object SendRequestPhone3(java.lang.String leadId, java.lang.String countryCode, java.lang.String number);
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;LSystem/String;)LRestSharp/IRestResponse;")
    public native system.Object SendRequestFirstName(java.lang.String leadId, java.lang.String firstName);
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;LSystem/String;)LRestSharp/IRestResponse;")
    public native system.Object SendRequestLastName(java.lang.String leadId, java.lang.String lastName);
    
    public static system.Type typeof() {
        return apitestframework.requests.publicapirequests.PublicApiLead.staticType;
    }
    
    private static void InitJNI(net.sf.jni4net.inj.INJEnv env, system.Type staticType) {
        apitestframework.requests.publicapirequests.PublicApiLead.staticType = staticType;
    }
    //</generated-proxy>
}
