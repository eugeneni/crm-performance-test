// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by jni4net. See http://jni4net.sourceforge.net/ 
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

package apitestframework.requests.publicapirequests;

@net.sf.jni4net.attributes.ClrType
public class PublicApiTradingAccounts extends system.Object {
    
    //<generated-proxy>
    private static system.Type staticType;
    
    protected PublicApiTradingAccounts(net.sf.jni4net.inj.INJEnv __env, long __handle) {
            super(__env, __handle);
    }
    
    @net.sf.jni4net.attributes.ClrConstructor("()V")
    public PublicApiTradingAccounts() {
            super(((net.sf.jni4net.inj.INJEnv)(null)), 0);
        apitestframework.requests.publicapirequests.PublicApiTradingAccounts.__ctorPublicApiTradingAccounts0(this);
    }
    
    @net.sf.jni4net.attributes.ClrMethod("()V")
    private native static void __ctorPublicApiTradingAccounts0(net.sf.jni4net.inj.IClrProxy thiz);
    
    @net.sf.jni4net.attributes.ClrMethod("()LRestSharp/IRestResponse;")
    public native system.Object SendRequestLeadInfo();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)LRestSharp/IRestResponse;")
    public native system.Object SendRequestCreateAccount(java.lang.String platformType);
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;LSystem/String;)LRestSharp/IRestResponse;")
    public native system.Object SendRequestNickname(java.lang.String accountId, java.lang.String nickname);
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)LRestSharp/IRestResponse;")
    public native system.Object SendRequestCurrency(java.lang.String accountId);
    
    public static system.Type typeof() {
        return apitestframework.requests.publicapirequests.PublicApiTradingAccounts.staticType;
    }
    
    private static void InitJNI(net.sf.jni4net.inj.INJEnv env, system.Type staticType) {
        apitestframework.requests.publicapirequests.PublicApiTradingAccounts.staticType = staticType;
    }
    //</generated-proxy>
}
