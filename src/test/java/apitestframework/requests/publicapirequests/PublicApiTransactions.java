// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by jni4net. See http://jni4net.sourceforge.net/ 
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

package apitestframework.requests.publicapirequests;

@net.sf.jni4net.attributes.ClrType
public class PublicApiTransactions extends system.Object {
    
    //<generated-proxy>
    private static system.Type staticType;
    
    protected PublicApiTransactions(net.sf.jni4net.inj.INJEnv __env, long __handle) {
            super(__env, __handle);
    }
    
    @net.sf.jni4net.attributes.ClrConstructor("()V")
    public PublicApiTransactions() {
            super(((net.sf.jni4net.inj.INJEnv)(null)), 0);
        apitestframework.requests.publicapirequests.PublicApiTransactions.__ctorPublicApiTransactions0(this);
    }
    
    @net.sf.jni4net.attributes.ClrMethod("()V")
    private native static void __ctorPublicApiTransactions0(net.sf.jni4net.inj.IClrProxy thiz);
    
    @net.sf.jni4net.attributes.ClrMethod("(LApiTestFramework/Models/Account;LSystem/String;LSystem/String;LSystem/String;LSystem/String;)LRestSharp/IRestResponse;")
    public native system.Object SendRequestAccountId(apitestframework.models.Account account, java.lang.String startDate, java.lang.String endDate, java.lang.String from, java.lang.String to);
    
    @net.sf.jni4net.attributes.ClrMethod("(LApiTestFramework/Models/Account;LSystem/String;LSystem/String;LSystem/String;LSystem/String;)LRestSharp/IRestResponse;")
    public native system.Object SendRequestPdf(apitestframework.models.Account account, java.lang.String startDate, java.lang.String endDate, java.lang.String from, java.lang.String to);
    
    public static system.Type typeof() {
        return apitestframework.requests.publicapirequests.PublicApiTransactions.staticType;
    }
    
    private static void InitJNI(net.sf.jni4net.inj.INJEnv env, system.Type staticType) {
        apitestframework.requests.publicapirequests.PublicApiTransactions.staticType = staticType;
    }
    //</generated-proxy>
}
