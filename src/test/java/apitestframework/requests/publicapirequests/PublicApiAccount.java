// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by jni4net. See http://jni4net.sourceforge.net/ 
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

package apitestframework.requests.publicapirequests;

@net.sf.jni4net.attributes.ClrType
public class PublicApiAccount extends system.Object {
    
    //<generated-proxy>
    private static system.Type staticType;
    
    protected PublicApiAccount(net.sf.jni4net.inj.INJEnv __env, long __handle) {
            super(__env, __handle);
    }
    
    @net.sf.jni4net.attributes.ClrConstructor("()V")
    public PublicApiAccount() {
            super(((net.sf.jni4net.inj.INJEnv)(null)), 0);
        apitestframework.requests.publicapirequests.PublicApiAccount.__ctorPublicApiAccount0(this);
    }
    
    @net.sf.jni4net.attributes.ClrMethod("()V")
    private native static void __ctorPublicApiAccount0(net.sf.jni4net.inj.IClrProxy thiz);
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;LSystem/String;)LRestSharp/IRestResponse;")
    public native system.Object SendRequestLogon(java.lang.String email, java.lang.String password);
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)LRestSharp/IRestResponse;")
    public native system.Object SendRequestLogonWithToken(java.lang.String token);
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)LRestSharp/IRestResponse;")
    public native system.Object SendRequestSetAuthCookie(java.lang.String clientId);
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;)LSystem/String;")
    public native java.lang.String SendRequestGotoTradingPlatform(java.lang.String accountId);
    
    @net.sf.jni4net.attributes.ClrMethod("()LRestSharp/IRestResponse;")
    public native system.Object SendRequestKeepAlive();
    
    @net.sf.jni4net.attributes.ClrMethod("()LRestSharp/IRestResponse;")
    public native system.Object SendRequestLogoff();
    
    @net.sf.jni4net.attributes.ClrMethod("()LApiTestFramework/Models/PublicApi/LeadMyAcc;")
    public native apitestframework.models.publicapi.LeadMyAcc SendRequestLeadGet();
    
    @net.sf.jni4net.attributes.ClrMethod("(LApiTestFramework/Models/ModelsForMethodsData/PublicApi/DataPublicApiAccountLead;)LRestSharp/IRestResponse;")
    public native system.Object SendRequestLeadPut(apitestframework.models.modelsformethodsdata.publicapi.DataPublicApiAccountLead data);
    
    @net.sf.jni4net.attributes.ClrMethod("(LApiTestFramework/Users/Lead/Client;)LRestSharp/IRestResponse;")
    public native system.Object SendRequestCompleteRegistration(apitestframework.users.lead.Client client);
    
    @net.sf.jni4net.attributes.ClrMethod("()LRestSharp/IRestResponse;")
    public native system.Object SendRequestSignLink();
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;LSystem/String;)LRestSharp/IRestResponse;")
    public native system.Object SendRequestChangePassword(java.lang.String oldPassword, java.lang.String newPassword);
    
    @net.sf.jni4net.attributes.ClrMethod("(LSystem/String;LSystem/String;LSystem/String;)LRestSharp/IRestResponse;")
    public native system.Object SendRequestForgotPassword(java.lang.String email, java.lang.String baseLink, java.lang.String culture);
    
    @net.sf.jni4net.attributes.ClrMethod("(LApiTestFramework/Users/Lead/Client;LSystem/String;LSystem/String;)LRestSharp/IRestResponse;")
    public native system.Object SendRequestResetPassword(apitestframework.users.lead.Client client, java.lang.String id, java.lang.String newPassword);
    
    @net.sf.jni4net.attributes.ClrMethod("()LRestSharp/IRestResponse;")
    public native system.Object SendRequestActivity();
    
    public static system.Type typeof() {
        return apitestframework.requests.publicapirequests.PublicApiAccount.staticType;
    }
    
    private static void InitJNI(net.sf.jni4net.inj.INJEnv env, system.Type staticType) {
        apitestframework.requests.publicapirequests.PublicApiAccount.staticType = staticType;
    }
    //</generated-proxy>
}
