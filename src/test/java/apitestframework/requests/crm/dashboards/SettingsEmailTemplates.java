// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by jni4net. See http://jni4net.sourceforge.net/ 
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

package apitestframework.requests.crm.dashboards;

@net.sf.jni4net.attributes.ClrType
public class SettingsEmailTemplates extends system.Object {
    
    //<generated-proxy>
    private static system.Type staticType;
    
    protected SettingsEmailTemplates(net.sf.jni4net.inj.INJEnv __env, long __handle) {
            super(__env, __handle);
    }
    
    @net.sf.jni4net.attributes.ClrConstructor("()V")
    public SettingsEmailTemplates() {
            super(((net.sf.jni4net.inj.INJEnv)(null)), 0);
        apitestframework.requests.crm.dashboards.SettingsEmailTemplates.__ctorSettingsEmailTemplates0(this);
    }
    
    @net.sf.jni4net.attributes.ClrMethod("()V")
    private native static void __ctorSettingsEmailTemplates0(net.sf.jni4net.inj.IClrProxy thiz);
    
    @net.sf.jni4net.attributes.ClrMethod("()LApiTestFramework/Models/DictionariesPages/DictionariesTemplatesPage;")
    public native apitestframework.models.dictionariespages.DictionariesTemplatesPage GetDictionaries();
    
    @net.sf.jni4net.attributes.ClrMethod("([LApiTestFramework/Models/DictionariesPages/TypeTemplate;LSystem/String;)LApiTestFramework/Models/DictionariesPages/TypeTemplate;")
    public native apitestframework.models.dictionariespages.TypeTemplate GetTypeTemplate(apitestframework.models.dictionariespages.TypeTemplate[] templates, java.lang.String templatename);
    
    public static system.Type typeof() {
        return apitestframework.requests.crm.dashboards.SettingsEmailTemplates.staticType;
    }
    
    private static void InitJNI(net.sf.jni4net.inj.INJEnv env, system.Type staticType) {
        apitestframework.requests.crm.dashboards.SettingsEmailTemplates.staticType = staticType;
    }
    //</generated-proxy>
}
